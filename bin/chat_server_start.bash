#!/bin/bash

SERVER_DIR=/home/repro/reprohealth-server/chat


# Init the environment
cd $SERVER_DIR
git pull origin staging

# Prepare Environment
rm -rf node_modules/
npm install
gulp build-dist
kill -9 $(lsof -t -i:9995)

export NEW_RELIC_ENVIRONMENT=staging
export NEW_RELIC_APP_NAME="ReproHealth Chat"

node src/server/main.js