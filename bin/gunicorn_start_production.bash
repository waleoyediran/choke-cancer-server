#!/bin/bash

NAME="ReproHealth"
DJANGODIR=/home/reprohealth/reprohealth-server
USER=reprohealth
GROUP=apps
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=reprohealth.settings.production
DJANGO_WSGI_MODULE=reprohealth.wsgi
PORT=9000
NEW_RELIC_CONFIG_FILE=/home/reprohealth/newrelic.ini
NEW_RELIC_ENVIRONMENT=production

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
git pull origin master

# Prepare Environment
source env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
export NEW_RELIC_CONFIG_FILE=$NEW_RELIC_CONFIG_FILE
export NEW_RELIC_ENVIRONMENT=$NEW_RELIC_ENVIRONMENT

# Install Requirements
pip install -r requirements.txt

# Collect Static Files
python manage.py collectstatic --no-input

# Run migrations
python manage.py makemigrations
python manage.py makemigrations api
python manage.py migrate

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec newrelic-admin run-program gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --user=$USER --group=$GROUP \
  --bind=127.0.0.1:$PORT \
  --config /etc/gunicorn.d/gunicorn.py \
  --log-level=debug \
  --log-file=-