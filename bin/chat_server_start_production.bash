#!/bin/bash

SERVER_DIR=/home/reprohealth/reprohealth-server/chat


# Init the environment
cd $SERVER_DIR
git pull origin master

# Prepare Environment
rm -rf node_modules/
npm install
gulp build-dist
kill -9 $(lsof -t -i:9995)

export NEW_RELIC_ENVIRONMENT=production
export NEW_RELIC_APP_NAME="ReproHealth Chat (Production)"

node src/server/main.js