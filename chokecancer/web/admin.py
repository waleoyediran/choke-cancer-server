from django.contrib.sites.shortcuts import get_current_site
from django.template.response import TemplateResponse
from django.views.decorators.cache import never_cache
from django.utils.translation import ugettext as _
from rest_framework.renderers import JSONRenderer

from forms import StaffAuthenticationForm
from ckeditor.widgets import CKEditorWidget
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from chokecancer.api.models import *
from chokecancer.api.serializers import UserSerializer


class ProfileAdmin(admin.ModelAdmin):
    pass


class PostAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    empty_value_display = '-empty-'
    exclude = ('slug', "content_text")
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }


class TagAdmin(admin.ModelAdmin):
    pass


@never_cache
def chat(request, template_name='admin/chat.html', extra_context=None):
    """
    Displays the chat.
    :param extra_context:
    :param template_name:
    :param request:
    """
    users = User.objects.all().order_by('id')
    users_ser = UserSerializer(users, context={'request': request}, many=True)
    users_json = JSONRenderer().render(users_ser.data)

    current_site = get_current_site(request)

    context = {
        'site': current_site,
        'site_name': current_site.name,
        'app_id': settings.LAYER_APP_ID,
        'users_list': users_json,
        'spika_base_url': settings.SPIKA_BASE_URL,
        'user_json': JSONRenderer().render(UserSerializer(request.user, context={'request': request}).data)
    }

    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


class ReproAdmin(AdminSite):
    site_header = "ChokeCancer Administration"
    index_title = "Admin"
    site_title = "Admin"

    login_form = StaffAuthenticationForm

    def get_urls(self):
        from django.conf.urls import url

        urls = super(ReproAdmin, self).get_urls()
        urls += [
            url(r'^chat/$', self.admin_view(self.chat), name='support_chat'),
            url(r'^chat2/$', self.admin_view(self.chat2), name='support_chat2')
        ]
        return urls

    @never_cache
    def chat(self, request):
        context = dict(self.each_context(request),
                       title=_('Chat Service'),
                       app_path=request.get_full_path(),
                       )
        defaults = {
            'extra_context': context,
            'template_name': 'admin/chat.html',
        }
        request.current_app = self.name
        return chat(request, **defaults)

    @never_cache
    def chat2(self, request):
        context = dict(self.each_context(request),
                       title=_('Chat Service'),
                       app_path=request.get_full_path(),
                       )
        defaults = {
            'extra_context': context,
            'template_name': 'admin/chat2.html',
        }
        request.current_app = self.name
        return chat(request, **defaults)

    def has_permission(self, request):
        """
        Add check for is_staff.
        :param request:
        """
        return request.user.is_active and request.user.profile.is_staff


# Secondary Admin Site
repro_admin = ReproAdmin(name="repro-admin")
repro_admin.register(Post, PostAdmin)
repro_admin.register(Tag, TagAdmin)
repro_admin.register(Profile, ProfileAdmin)

# Main Admin site
admin.site.register(Tag, TagAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Profile, ProfileAdmin)
