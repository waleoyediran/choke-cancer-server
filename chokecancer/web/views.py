from django.views.generic.base import TemplateView


class HomePageView(TemplateView):
    template_name = 'web/index.html'


class AppPrivacyPageView(TemplateView):
    template_name = 'web/app/privacypolicy.htm'


class AppTermsPageView(TemplateView):
    template_name = 'web/app/terms.html'


class ConfirmEmailView(TemplateView):
    template_name = 'web/emails/confirm_email.html'

    def get_context_data(self, **kwargs):
        context = super(ConfirmEmailView, self).get_context_data(**kwargs)
        if 'key' in self.kwargs:
            context['key'] = self.kwargs['key']
        else:
            raise Exception('The confirmation token is absent')

        return context
