from django.conf.urls import url
from chokecancer.web.views import *

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^app/terms$', AppTermsPageView.as_view(), name='terms'),
    url(r'^app/privacy$', AppPrivacyPageView.as_view(), name='privacy')
]
