from base import *

DEBUG = True

ALLOWED_HOSTS = ['*']
STATIC_ROOT = '/Users/oyewale/static'
INSTALLED_APPS += [
    'debug_toolbar',
    'django_extensions',
]
