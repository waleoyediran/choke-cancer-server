from django.conf.urls import url
from rest_framework import routers
from chokecancer.api import views
from rest_framework.urlpatterns import format_suffix_patterns

api_router = routers.DefaultRouter()
api_router.register(r'users/staff', views.StaffViewSet)
api_router.register(r'users', views.UserViewSet)
api_router.register(r'posts', views.PostViewSet)

urlpatterns = [
    url(r'identity_token/$', views.LayerIdentityView.as_view(), name='identity_token'),
    url(r'healthcare/$', views.PlacesAPIView.as_view(), name='healthcare'),
    url(r'rooms/$', views.RoomsAPIView.as_view(), name='rooms'),
    url(r'chat/$', views.ChatAPIView.as_view(), name='chat'),
]
api_extra_urlpatterns = format_suffix_patterns(urlpatterns)
