
import time
import datetime

import pytz
from django.contrib.auth.models import User
from rest_auth.models import TokenModel
from rest_auth.registration.serializers import RegisterSerializer

from chokecancer.api.models import Post
from rest_framework import serializers


class TimestampField(serializers.DateTimeField):
    def to_internal_value(self, data):
        return datetime.datetime.fromtimestamp(int(data), tz=pytz.timezone('Africa/Lagos'))

    def to_representation(self, value):
        return int(time.mktime(value.timetuple()))


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name')
        extra_kwargs = {'password': {'write_only': True}}


class PostSerializer(serializers.ModelSerializer):
    created = TimestampField(read_only=True)
    updated = TimestampField(read_only=True)
    author = UserSerializer()

    class Meta:
        model = Post
        fields = ('id', 'title','url', 'featured_image','author', 'content', 'content_text', 'created', 'updated')


class PlaceSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class AuthSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = TokenModel
        fields = ('key', 'user')


class RegistrationSerializer(RegisterSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)

    def get_cleaned_data(self):
        return {
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', '')
        }
