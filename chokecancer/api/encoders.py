import json
from decimal import Decimal

from googleplaces import Place


class PlaceEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Place):
            place = {
                'place_id': obj.place_id,
                'location': obj.geo_location,
                'name': obj.name,
                'icon': obj.icon
            }
            return place
        elif isinstance(obj, Decimal):
            return float(obj)

        return json.JSONEncoder.default(self, obj)
