import json

from rest_framework.renderers import BaseRenderer

from encoders import PlaceEncoder


class PlaceRenderer(BaseRenderer):
    media_type = 'application/json'
    format = 'json'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        return json.dumps(data, cls=PlaceEncoder)