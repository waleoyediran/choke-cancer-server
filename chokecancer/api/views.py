import datetime


from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from django.conf import settings
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from googleplaces import GooglePlaces, types
from pymongo.mongo_client import MongoClient

from renderers import PlaceRenderer
from chokecancer.api.lib.layer import generate_identity_token
from chokecancer.api.models import Post
from chokecancer.api.serializers import UserSerializer, PostSerializer
from rest_auth.registration.views import SocialLoginView
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class UnsafeSessionAuthentication(SessionAuthentication):
    def authenticate(self, request):
        http_request = request._request
        user = getattr(http_request, 'user', None)

        if not user or not user.is_active:
            return None

        return user, None


class LayerIdentityView(APIView):
    authentication_classes = [TokenAuthentication, UnsafeSessionAuthentication]
    permission_classes = [IsAuthenticated]

    @csrf_exempt
    def post(self, request, format=None):
        if request.POST.get('nonce'):
            # create the token
            identity_token = generate_identity_token(str(request.user.id), request.POST['nonce'])
            content = {
                'identity_token': identity_token,
            }
        else:
            print request.POST
            raise ParseError("No nonce present")
        return Response(content)


class PlacesAPIView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [AllowAny, ]
    renderer_classes = [PlaceRenderer, ]

    def get(self, request, format=None):
        if request.GET['coords']:
            # create the token
            coords = request.GET['coords']
            lat, lng = coords.split(",")
            google_places = GooglePlaces(settings.PLACES_API_KEY)

            query_result = google_places.nearby_search(
                    lat_lng={"lat": lat, "lng": lng},
                    radius=20000, types=[types.TYPE_HOSPITAL, types.TYPE_HEALTH])
        else:
            raise ParseError()
        # return HTTPResponse
        return Response(query_result.places)


class RoomsAPIView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, ]

    def get(self, request, format=None):
        queryset = User.objects.filter(profile__is_staff=1).exclude(username=request.user.username)
        rooms = list()
        for u in queryset:
            room = {
                'name': u.username + '--' + request.user.username,
                'avatar': 'https://storage.googleapis.com/reprohealth-app/avatar/support-avatar.png',
                'title': u.username
            }
            rooms.append(room)

        return Response(rooms)


class ChatAPIView(APIView):
    authentication_classes = [SessionAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated, ]

    def get(self, request, format=None):
        client = MongoClient('localhost', 27017)
        db = client['repro-chat']

        username = request.user.username
        regex = "^" + username + "--"
        chat = db['spika_messages'].aggregate([
            {"$match": {"roomID": {"$regex": regex}}},
            {"$sort": {"created": -1}},
            {"$group": {"_id": "$roomID"}}
        ])
        rooms = list()
        for a in chat:
            room = {
                "id": a["_id"],
                "name": a["_id"].split("--")[1]
            }
            rooms.append(room)

        return Response(rooms)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class StaffViewSet(viewsets.ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = User.objects.filter(profile__is_staff=1)
    serializer_class = UserSerializer

    def get_queryset(self):
        """
        Optionally filters by url-params
        :return:
        """
        queryset = self.queryset | User.objects.filter(id=self.request.user.id)

        return queryset


class PostViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    authentication_classes = [TokenAuthentication]
    permission_classes = [AllowAny]
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        """
        Optionally filters by url-params
        :return:
        """
        queryset = self.queryset
        since = self.request.query_params.get('since', None)
        if since is not None and len(since.strip()) > 0:
            queryset = queryset.filter(updated__gt=datetime.datetime.fromtimestamp(long(since))).order_by('updated')
        else:
            queryset = queryset.order_by('-created')

        return queryset
