from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from utils.text_utils import get_summary


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    def __unicode__(self):
        return self.user.username + "`s Profile"


class Tag(models.Model):
    name = models.CharField(max_length=16, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Post(models.Model):

    title = models.CharField(max_length=64, db_index=True)
    slug = models.SlugField(unique=True)
    featured_image = models.ImageField(null=True, upload_to='photo/%Y/%m/%d')
    content = models.TextField()
    content_text = models.TextField()
    tags = models.ManyToManyField(Tag)
    author = models.ForeignKey(User, on_delete=models.SET(1))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        self.content_text = get_summary(self.content)
        super(Post, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    class Meta:
        get_latest_by = 'created'
        ordering = ['created']
