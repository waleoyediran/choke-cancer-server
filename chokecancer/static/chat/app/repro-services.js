/**
 * Created by oyewale on 13/07/2016.
 */

/* global layer */
'use strict';

/**
 * Exports ChokeCancer Service Utilities
 * * window.chatApp.Services:
 *   * getIdentityToken: Gets Identity Token from Identity Server
 */
(function() {
    var app = window.chatApp;

    /**
     * Get user by id
     * @param value
     */
    function getUserById(value) {
      for (var i=0, iLen=users.length; i<iLen; i++) {
        if (users[i].id == value) return users[i];
      }
    }

    app.Services = {
        getUserById: getUserById
    };
})();

