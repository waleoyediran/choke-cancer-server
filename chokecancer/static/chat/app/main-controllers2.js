/* global angular layer */
'use strict';

var sampleControllers = angular.module('sampleAppControllers', []);

/**
 * Notes: data in this application is driven by Queries.  Queries can be set
 * to return Conversation and Message Objects or Instances.  An Object
 * lets us have an immutable object that simplifies angular's scope comparisons.
 * An Instance provides methods that let us interact with the layer services.
 * Both are used in this application: Objects for managing angular state, Instances for interacting.
 */

var identityReady;
var loadClients;

window.chatApp = {
    appId: appId,
    user: user,
    //sessionToken: sessionToken
};


/**
 * The Main Application Controller initializes the client
 * and loads the Chat Controller
 */
sampleControllers.controller('appCtrl', function ($scope, $http) {
    $scope.appCtrlState = {
        isReady: false,
        client: null
    };

    $scope.chat = {
        rooms: []
    };

    loadClients = function() {
        $http({
            method: 'GET',
            url: '/api/chat/'
        }).success(function (data) {
            $scope.chat.rooms = data;
            $scope.appCtrlState.isReady = true;
            console.log($scope.chat.rooms);
            loadingScreen.finish();
        });
    };

    /**
     * Start by creating a client; it will authenticate asynchronously,
     * so the UI needs to account for the fact that it won't have any data
     * when first rendering.
     */
    identityReady = function() {

        /**
         * Initialize Layer Client with `appId`
         */
        $scope.appCtrlState.client = new layer.Client({
            appId: window.chatApp.appId,
            isTrustedDevice: true,
            userId: window.chatApp.user.id
        });
    };

    //angular.element(document).ready(function () {
    //    //identityReady();
    //    loadClients();
    //});
    loadClients();
});

/**
 * The Main Application Controller manages:
 * 1. Whether to show the userList or the messages panel
 * 2. All routing (current conversation or new conversation)
 */
sampleControllers.controller('chatCtrl', function ($scope, $route, $location) {
    $scope.chatCtrlState = {
        showUserList: false,
        currentConversation: null
    };

    /**
     * Get the Conversation from cache... or from the server if not cached.
     * Once loaded, set it as our currentConversation.
     */
    $scope.loadConversation = function loadConversation(id) {
        var conversation = $scope.appCtrlState.client.getConversation(id, true);
        conversation.once('conversations:loaded', function() {
            $scope.chatCtrlState.currentConversation = conversation.toObject();
            $scope.$digest();
            document.querySelectorAll('.message-textarea')[0].focus();
        });
    };

    /*
     * Whenever the url changes, Load the requested Conversation
     * (if any), update our state and render
     */
    function handleLocation() {
        var matches = $location.$$url.match(/\/conversations\/.{8}-.{4}-.{4}-.{4}-.{12}$/);

        // If the url matches a Conversation URL, show it.
        if (matches) {
            if ($scope.appCtrlState.isReady) {
                $scope.loadConversation('layer://' + matches[0]);
            } else {
                $scope.$watch('appCtrlState.isReady', function() {
                    $scope.loadConversation('layer://' + matches[0]);
                });
            }
            $scope.chatCtrlState.showUserList = false;
        }

        // If the URL matches a new Conversation, show the Create Conversation UI
        // and insure all checkboxes are clear.
        else if ($location.$$url.match(/\/conversations\/new$/)) {
            $scope.chatCtrlState.showUserList = true;
            $scope.chatCtrlState.currentConversation = null;
            Array.prototype.slice.call(document.querySelectorAll('.user-list :checked'))
                .forEach(function(node) {
                    node.checked = false;
                });
        }
    }

    $scope.$on('$locationChangeSuccess', handleLocation);

    // Handle the initial url state we loaded the app with.
    handleLocation();


});

/**
 * The Conversation List Controller manages the List of Conversation on
 * the left column.
 *
 * This Controller manages the Conversation Query which delivers
 * all Conversations and any changes to the Conversations.
 *
 * Rendering is done by iterating over $scope.query.data
 */
sampleControllers.controller('conversationListCtrl', function ($scope, $rootScope) {
    // Once we are authenticated and the User list is loaded, create the query
    $scope.$watch('appCtrlState.isReady', function(newValue) {
        if (newValue) {

            // Create the Conversation List query
            //$scope.query = $scope.appCtrlState.client.createQuery({
            //    model: layer.Query.Conversation,
            //    dataType: 'object',
            //    paginationWindow: 500
            //});

            /**
             * Any time the query data changes, rerender.  Data changes when:
             *
             * * The Conversation data has loaded from the server
             * * A new Conversation is created and added to the results
             * * A Conversation is deleted and removed from the results
             * * Any Conversation in the results has a change of metadata or participants
             */
            //$scope.query.on('change', function() {
            //    $rootScope.$digest();
            //});
        }
    }, this);

    /**
     * This deletes a Conversation from the server.
     */
    $scope.deleteConversation = function(conversation) {
        var conversationInstance = $scope.appCtrlState.client.getConversation(conversation.id);
        if (conversationInstance) {
            window.setTimeout(function() {
                if (confirm('Are you sure you want to delete this conversation?')) {
                    conversationInstance.delete(layer.Constants.DELETION_MODE.ALL);
                }
            }, 1);
        }
    };

    $scope.setConversation = function(room) {
        console.log(window.chatApp.user.username, window.chatApp.user.id.toString(), room.id);

        $scope.chatCtrlState.currentConversation = room;

        SpikaAdapter.attach({

            spikaURL: spikaBaseUrl + '/repro-spika',
            attachTo : document.getElementsByClassName('right-panel')[0], // id or div to show into
            user : {
                id : window.chatApp.user.id.toString(),
                name : window.chatApp.user.username,
                avatarURL : 'https://storage.googleapis.com/reprohealth-app/avatar/support-avatar.png',
                roomID : room.id,
                role: "admin"
            },
            config : {
                apiBaseUrl : spikaBaseUrl + '/repro-spika' + "/v1",
                socketUrl : spikaBaseUrl + '/repro-spika',
                showSidebar : false,
                showTitlebar : false
            },
            listener : {

                onPageLoad: function(){

                },
                onNewMessage:function(obj){

                },
                onNewUser:function(obj){

                },
                onUserLeft:function(obj){

                },
                OnUserTyping:function(obj){

                },
                OnMessageChanges:function(obj){

                },
                onOpenMessage:function(obj){
                    return true;
                },
                OnOpenFile:function(obj){
                    return true;
                }
            }

        });
    };
});
