/* global angular layer */
'use strict';

var controllers = angular.module('conversationPanelControllers', []);

/**
 * The Conversation Controller manages:
 *
 * * An editable titlebar
 * * A scrollable/pagable list of Messages
 * * A SendPanel for composing and sending Messages
 */
controllers.controller('conversationCtrl', function($scope) {

});

/**
 * The Typing Indicator Controller renders if anyone is typing
 */
controllers.controller('typingIndicatorCtrl', function($scope) {
    $scope.typing = [];
    $scope.paused = [];

    // Insure that when we change conversations we don't carry
    // typing state over from last Conversation
    $scope.$watch('chatCtrlState.currentConversation', function() {
        $scope.typing = [];
        $scope.paused = [];
    });


    // Render a typing indicator message
    $scope.getText = function() {
        var users = $scope.typing;
        if (users && users.length > 0) {
            return users.length === 1 ? users[0] + ' is typing' : users.join(', ') + ' are typing';
        } else {
            return '';
        }
    };
});

/**
 * The Message List Controller renders a list of Messages,
 * and provides pagination
 */
controllers.controller('messageListCtrl', function ($scope) {
    // Store all message data here
    $scope.data = [];

    // Property prevents paging; useful if we are mid-pagination operation.
    $scope.disablePaging = true;


    /**
     * Whenever new messages arrive:
     *
     * * Flag them as read which will tell the server that they are read
     * * Append the results to our data
     *
     * See http://static.layer.com/sdk/docs/#!/api/layer.Query for
     * more information on query change events.
     */


    /**
     * Get initials from sender
     *
     * @method
     * @param  {Object} message - Message object or instance
     * @return {string} - User's display name
     */
    $scope.getSenderInitials = function(message) {
        var name = message.sender.userId || 'Unknown';
        return window.chatApp.Services.getUserById(name).username.substr(0, 2).toUpperCase();
    };


    $scope.getUserNameById = function(userId) {
        return window.chatApp.Services.getUserById(userId).username;
    };

    /**
     * Get the message's read/delivered status.  For this
     * simple example we ignore delivered (see `message.deliveryStatus`).
     *
     * See http://static.layer.com/sdk/docs/#!/api/layer.Message-property-readStatus
     *
     * @method
     * @param  {Object} message - Message object or instance
     * @return {string} - Message to display in the status node
     */
    $scope.getMessageStatus = function(message) {
        switch (message.readStatus) {
            case 'NONE':
                return 'unread';
            case 'SOME':
                return 'read by some';
            case 'ALL':
                return 'read';
        }
    };

    /**
     * Get the message sentAt string in a nice renderable format.
     *
     * See http://static.layer.com/sdk/docs/#!/api/layer.Message-property-sentAt
     *
     * @method
     * @param  {Object} message - Message object or instance
     * @return {string} - Message to display in the sentAt node
     */
    $scope.getMessageDate = function(message) {
        return window.layerSample.dateFormat(message.sentAt);
    };

    /**
     * nextPage() is called by the infinite scroller each
     * time it wants another page of messages from the server
     * to render.
     */
    $scope.nextPage = function() {
    };
});
