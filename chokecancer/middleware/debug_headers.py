import re


class TokenDebuggerMiddleware(object):

    def process_request(self, request):
        regex_http_ = re.compile(r'^HTTP_.+$')

        request_headers = {}
        for header in request.META:
            if regex_http_.match(header):
                request_headers[header] = request.META[header]
        print request_headers
