
from chokecancer.api.lib import layer


class LayerSessionMiddleware(object):

    def process_response(self, request, response):
        if 'admin/login' in request.path and request.method == 'POST' and request.user.is_authenticated():
            try:
                request.session['layer_session_token'] = layer.get_session_token(str(request.user.id))
            except Exception, e:
                print str(e)

        return response
