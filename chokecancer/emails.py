from emailtools.cbe import MarkdownEmail
from emailtools.cbe.mixins import UserTokenEmailMixin


class PasswordResetEmail(UserTokenEmailMixin, MarkdownEmail):
    from_email = 'admin@example.com'
    template_name = 'registration/password_reset_email.html'
    subject = "Password Reset"

    def get_to(self):
        return [self.get_user().email]

    def get_context_data(self, **kwargs):
        kwargs = super(PasswordResetEmail, self).get_context_data()
        user = self.get_user()
        kwargs.update({
            'user': user,
            'reset_url': self.reverse_token_url('password_reset_confirm'),
        })
        return kwargs

send_password_reset_email = PasswordResetEmail.as_callable()