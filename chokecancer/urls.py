"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from chokecancer.web.admin import repro_admin
from chokecancer.api.urls import api_router, api_extra_urlpatterns
from chokecancer.api.views import FacebookLogin, GoogleLogin
from django.contrib import admin
from chokecancer.web.views import ConfirmEmailView

urlpatterns = [
    url(r'^api/', include(api_router.urls)),
    url(r'^api/', include(api_extra_urlpatterns)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(), name='account_confirm_email'),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^rest-auth/google/$', GoogleLogin.as_view(), name='google_login'),
    url(r'^accounts/', include('allauth.socialaccount.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^staff/', repro_admin.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^', include('chokecancer.web.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
